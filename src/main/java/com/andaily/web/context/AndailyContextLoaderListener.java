package com.andaily.web.context;

import com.andaily.infrastructure.support.LogHelper;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;

/**
 * @author Shengzhao Li
 */
public class AndailyContextLoaderListener extends ContextLoaderListener {

    private static LogHelper log = LogHelper.create(AndailyContextLoaderListener.class);

    @Override
    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);
        WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(event.getServletContext());
        BeanProvider.initialize(applicationContext);
        log.debug("Initial  BeanProvider successful, context is " + applicationContext);
    }
}